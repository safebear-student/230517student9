package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Admin on 23/05/2017.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin() {
        //Step1 Confirm we are on the Welcome Page
        assertTrue(this.welcomePage.checkCorrectPage());

        //Step2 click on the Login link and the Login Page loads
        assertTrue(welcomePage.clickOnlogin(this.loginPage));

        //Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage, "testuser", "testing"));
    }
}